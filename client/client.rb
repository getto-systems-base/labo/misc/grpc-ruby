require "grpc"
require "hello_world_services_pb"

module HelloWorld
  class << self
    def main
      stub = Greeter::Stub.new "192.168.0.104:30081", :this_channel_is_insecure
      response = stub.say_hello HelloRequest.new(name: "World")
      p "Greeting: #{response.message}"
    end
  end
end

HelloWorld.main
