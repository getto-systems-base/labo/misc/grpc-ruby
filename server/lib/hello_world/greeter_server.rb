require "hello_world_services_pb"

module HelloWorld
  class GreeterServer < Greeter::Service
    def say_hello(req, _call)
      HelloReply.new message: "Hello, #{req.name}"
    end
  end
end
